"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bignumber_js_1 = require("bignumber.js");
// Siacoin -> hastings unit conversion functions
// These make conversion between units of Sia easy and consistent for developers.
// Never return exponentials from BigNumber.toString, since they confuse the API
bignumber_js_1.BigNumber.config({ EXPONENTIAL_AT: 1e9 });
bignumber_js_1.BigNumber.config({ DECIMAL_PLACES: 30 });
// Hastings is the lowest divisible unit in Sia. This constant will be used to
// calculate the conversion between the base unit to human readable values.
const hastingsPerSiacoin = new bignumber_js_1.BigNumber('10').exponentiatedBy(24);
function toSiacoins(hastings) {
    return new bignumber_js_1.BigNumber(hastings).dividedBy(hastingsPerSiacoin);
}
exports.toSiacoins = toSiacoins;
function toHastings(siacoins) {
    return new bignumber_js_1.BigNumber(siacoins).times(hastingsPerSiacoin);
}
exports.toHastings = toHastings;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3kuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2N1cnJlbmN5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsK0NBQXlDO0FBRXpDLGdEQUFnRDtBQUNoRCxpRkFBaUY7QUFDakYsZ0ZBQWdGO0FBQ2hGLHdCQUFTLENBQUMsTUFBTSxDQUFDLEVBQUUsY0FBYyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7QUFDMUMsd0JBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztBQUV6Qyw4RUFBOEU7QUFDOUUsMkVBQTJFO0FBQzNFLE1BQU0sa0JBQWtCLEdBQUcsSUFBSSx3QkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUVuRSxTQUFnQixVQUFVLENBQUMsUUFBNEI7SUFDckQsT0FBTyxJQUFJLHdCQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLENBQUM7QUFDL0QsQ0FBQztBQUZELGdDQUVDO0FBRUQsU0FBZ0IsVUFBVSxDQUFDLFFBQTRCO0lBQ3JELE9BQU8sSUFBSSx3QkFBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0FBQzNELENBQUM7QUFGRCxnQ0FFQyJ9