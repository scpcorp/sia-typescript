export interface ClientConfig {
    apiHost?: string;
    apiPort?: number;
    hostPort?: number;
    rpcPort?: number;
    agent?: string;
    apiAuthentication?: 'auto' | boolean;
    apiAuthenticationPassword?: string;
    dataDirectory?: string;
    modules?: ModuleConfig;
}
export interface ModuleConfig {
    gateway: boolean;
    consensus: boolean;
    transactionPool: boolean;
    wallet: boolean;
    renter: boolean;
    host: boolean;
    miner: boolean;
    explorer: boolean;
}
export interface SiadFlags {
    agent?: string;
    'api-addr'?: string;
    'authenticate-api'?: boolean;
    'disable-api-security'?: boolean;
    'host-addr'?: string;
    modules?: string;
    'no-boostrap'?: boolean;
    profile?: string;
    'profile-directory'?: string;
    'rpc-addr'?: string;
    'scprime-directory'?: string;
    'temp-password'?: string;
}
