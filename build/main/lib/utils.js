"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const os_1 = __importDefault(require("os"));
const path_1 = __importDefault(require("path"));
/**
 * Works similarly to Object.assign, but checks properties for undefined or
 * null values, skipping them if detected.
 * @param target
 * @param sources
 */
function assignDefined(target, ...sources) {
    for (const source of sources) {
        for (const key of Object.keys(source)) {
            const val = source[key];
            if (val !== undefined && val !== null) {
                target[key] = val;
            }
        }
    }
    return target;
}
exports.assignDefined = assignDefined;
/**
 * Retrieves the API password using the SIA_API_PASSWORD env variable or
 * attempt to read the local dir with fs.
 */
function getSiaPassword() {
    try {
        let configPath;
        switch (process.platform) {
            case 'win32':
                configPath = path_1.default.join(process.env.LOCALAPPDATA, 'ScPrime');
                break;
            case 'darwin':
                configPath = path_1.default.join(os_1.default.homedir(), 'Library', 'Application Support', 'ScPrime');
                break;
            default:
                configPath = path_1.default.join(os_1.default.homedir(), '.scprime');
        }
        const password = process.env.SIA_API_PASSWORD
            ? process.env.SIA_API_PASSWORD
            : fs_1.default.readFileSync(path_1.default.join(configPath, 'apipassword')).toString();
        return password.trim() || '';
    }
    catch (err) {
        // if apipassword doesn't exist, we'll just return an emtpy string
        return '';
    }
}
exports.getSiaPassword = getSiaPassword;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL3V0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsNENBQW9CO0FBQ3BCLDRDQUFvQjtBQUNwQixnREFBd0I7QUFFeEI7Ozs7O0dBS0c7QUFDSCxTQUFnQixhQUFhLENBQUMsTUFBYyxFQUFFLEdBQUcsT0FBaUI7SUFDaEUsS0FBSyxNQUFNLE1BQU0sSUFBSSxPQUFPLEVBQUU7UUFDNUIsS0FBSyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3JDLE1BQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN4QixJQUFJLEdBQUcsS0FBSyxTQUFTLElBQUksR0FBRyxLQUFLLElBQUksRUFBRTtnQkFDckMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQzthQUNuQjtTQUNGO0tBQ0Y7SUFDRCxPQUFPLE1BQU0sQ0FBQztBQUNoQixDQUFDO0FBVkQsc0NBVUM7QUFFRDs7O0dBR0c7QUFDSCxTQUFnQixjQUFjO0lBQzVCLElBQUk7UUFDRixJQUFJLFVBQVUsQ0FBQztRQUNmLFFBQVEsT0FBTyxDQUFDLFFBQVEsRUFBRTtZQUN4QixLQUFLLE9BQU87Z0JBQ1YsVUFBVSxHQUFHLGNBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFzQixFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUN2RSxNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLFVBQVUsR0FBRyxjQUFJLENBQUMsSUFBSSxDQUNwQixZQUFFLENBQUMsT0FBTyxFQUFFLEVBQ1osU0FBUyxFQUNULHFCQUFxQixFQUNyQixVQUFVLENBQ1gsQ0FBQztnQkFDRixNQUFNO1lBQ1I7Z0JBQ0UsVUFBVSxHQUFHLGNBQUksQ0FBQyxJQUFJLENBQUMsWUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQ3JEO1FBQ0QsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0I7WUFDM0MsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCO1lBQzlCLENBQUMsQ0FBQyxZQUFFLENBQUMsWUFBWSxDQUFDLGNBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDckUsT0FBTyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDO0tBQzlCO0lBQUMsT0FBTyxHQUFHLEVBQUU7UUFDWixrRUFBa0U7UUFDbEUsT0FBTyxFQUFFLENBQUM7S0FDWDtBQUNILENBQUM7QUExQkQsd0NBMEJDIn0=