"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const fs_1 = __importDefault(require("fs"));
const http_1 = __importDefault(require("http"));
const request_promise_native_1 = __importDefault(require("request-promise-native"));
const flags_1 = require("./flags");
const utils_1 = require("./utils");
class Client {
    constructor(config = {}) {
        this.launch = (binPath) => {
            try {
                // Check if siad exists
                if (fs_1.default.existsSync(binPath)) {
                    // Create flags
                    const flags = flags_1.parseFlags(this.config);
                    // Set euid if avl
                    const opts = {};
                    if (process.geteuid) {
                        opts.uid = process.geteuid();
                    }
                    this.process = child_process_1.spawn(binPath, flags, opts);
                    return this.process;
                }
                else {
                    throw new Error('could not find binary file in filesystem');
                }
            }
            catch (e) {
                throw new Error(e);
            }
        };
        this.makeRequest = (endpoint, querystring, method = 'GET', timeout = 30000) => __awaiter(this, void 0, void 0, function* () {
            try {
                const requestOptions = this.mergeDefaultRequestOptions({
                    url: endpoint,
                    timeout,
                    qs: querystring,
                    method
                });
                const data = yield request_promise_native_1.default(requestOptions);
                return data;
            }
            catch (e) {
                throw e;
            }
        });
        this.call = (options) => {
            if (typeof options === 'string') {
                return this.makeRequest(options);
            }
            else {
                const endpoint = options.url;
                const method = options.method;
                const qs = options.qs || undefined;
                const timeout = options.timeout || undefined;
                return this.makeRequest(endpoint, qs, method, timeout);
            }
        };
        this.gateway = () => {
            return this.makeRequest('/gateway');
        };
        this.daemonVersion = () => {
            return this.makeRequest('/daemon/version');
        };
        this.daemonStop = () => {
            return this.makeRequest('/daemon/stop');
        };
        /**
         * checks if siad responds to a /version call.
         */
        this.isRunning = () => __awaiter(this, void 0, void 0, function* () {
            if (this.process) {
                try {
                    yield this.daemonVersion();
                    return true;
                }
                catch (e) {
                    return false;
                }
            }
            else {
                try {
                    yield this.daemonVersion();
                    return true;
                }
                catch (e) {
                    return false;
                }
            }
        });
        this.getConnectionUrl = () => {
            if (!this.config.apiAuthenticationPassword) {
                this.config.apiAuthenticationPassword = utils_1.getSiaPassword();
            }
            return `http://:${this.config.apiAuthenticationPassword}@${this.config.apiHost}:${this.config.apiPort}`;
        };
        this.mergeDefaultRequestOptions = (opts) => {
            // These are the default config sourced from the Sia Agent
            const defaultOptions = {
                baseUrl: this.getConnectionUrl(),
                headers: {
                    'User-Agent': this.config.agent || 'ScPrime-Agent'
                },
                json: true,
                pool: this.agent,
                timeout: 30000
            };
            const formattedOptions = Object.assign({}, defaultOptions, opts);
            return formattedOptions;
        };
        try {
            if (config.dataDirectory) {
                fs_1.default.existsSync(config.dataDirectory);
            }
            const defaultConfig = {
                apiAuthentication: 'auto',
                apiHost: 'localhost',
                apiPort: 4280,
                hostPort: 4282,
                rpcPort: 4281
            };
            this.config = Object.assign({}, defaultConfig, config);
            // If strategy is set to 'auto', attempt to read from default siapassword file.
            if (this.config.apiAuthentication === 'auto') {
                this.config.apiAuthenticationPassword = utils_1.getSiaPassword();
            }
            this.agent = new http_1.default.Agent({
                keepAlive: true,
                maxSockets: 30
            });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.Client = Client;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBLGlEQUFvRDtBQUNwRCw0Q0FBb0I7QUFDcEIsZ0RBQXdCO0FBRXhCLG9GQUF3QztBQUV4QyxtQ0FBcUM7QUFFckMsbUNBQXlDO0FBRXpDLE1BQWEsTUFBTTtJQVFqQixZQUFZLFNBQXVCLEVBQUU7UUEwQjlCLFdBQU0sR0FBRyxDQUFDLE9BQWUsRUFBZ0IsRUFBRTtZQUNoRCxJQUFJO2dCQUNGLHVCQUF1QjtnQkFDdkIsSUFBSSxZQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUMxQixlQUFlO29CQUNmLE1BQU0sS0FBSyxHQUFHLGtCQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUN0QyxrQkFBa0I7b0JBQ2xCLE1BQU0sSUFBSSxHQUFRLEVBQUUsQ0FBQztvQkFDckIsSUFBSSxPQUFPLENBQUMsT0FBTyxFQUFFO3dCQUNuQixJQUFJLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztxQkFDOUI7b0JBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxxQkFBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBRTNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztpQkFDckI7cUJBQU07b0JBQ0wsTUFBTSxJQUFJLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO2lCQUM3RDthQUNGO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwQjtRQUNILENBQUMsQ0FBQztRQUVLLGdCQUFXLEdBQUcsQ0FDbkIsUUFBc0IsRUFDdEIsV0FBZ0MsRUFDaEMsU0FBaUIsS0FBSyxFQUN0QixVQUFrQixLQUFLLEVBQ3ZCLEVBQUU7WUFDRixJQUFJO2dCQUNGLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQztvQkFDckQsR0FBRyxFQUFFLFFBQVE7b0JBQ2IsT0FBTztvQkFDUCxFQUFFLEVBQUUsV0FBVztvQkFDZixNQUFNO2lCQUNQLENBQUMsQ0FBQztnQkFDSCxNQUFNLElBQUksR0FBRyxNQUFNLGdDQUFFLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3RDLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixNQUFNLENBQUMsQ0FBQzthQUNUO1FBQ0gsQ0FBQyxDQUFBLENBQUM7UUFFSyxTQUFJLEdBQUcsQ0FBQyxPQUFtQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxPQUFPLE9BQU8sS0FBSyxRQUFRLEVBQUU7Z0JBQy9CLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNsQztpQkFBTTtnQkFDTCxNQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDO2dCQUM3QixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO2dCQUM5QixNQUFNLEVBQUUsR0FBRyxPQUFPLENBQUMsRUFBRSxJQUFJLFNBQVMsQ0FBQztnQkFDbkMsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sSUFBSSxTQUFTLENBQUM7Z0JBQzdDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQzthQUN4RDtRQUNILENBQUMsQ0FBQztRQUVLLFlBQU8sR0FBRyxHQUFHLEVBQUU7WUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQztRQUVLLGtCQUFhLEdBQUcsR0FBRyxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQztRQUVLLGVBQVUsR0FBRyxHQUFHLEVBQUU7WUFDdkIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzFDLENBQUMsQ0FBQztRQUVGOztXQUVHO1FBQ0ksY0FBUyxHQUFHLEdBQTJCLEVBQUU7WUFDOUMsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNoQixJQUFJO29CQUNGLE1BQU0sSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO29CQUMzQixPQUFPLElBQUksQ0FBQztpQkFDYjtnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFDVixPQUFPLEtBQUssQ0FBQztpQkFDZDthQUNGO2lCQUFNO2dCQUNMLElBQUk7b0JBQ0YsTUFBTSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQzNCLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNWLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2FBQ0Y7UUFDSCxDQUFDLENBQUEsQ0FBQztRQUVLLHFCQUFnQixHQUFHLEdBQVcsRUFBRTtZQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsRUFBRTtnQkFDMUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsR0FBRyxzQkFBYyxFQUFFLENBQUM7YUFDMUQ7WUFDRCxPQUFPLFdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsSUFDckQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUNkLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM1QixDQUFDLENBQUM7UUFFTSwrQkFBMEIsR0FBRyxDQUNuQyxJQUF1QixFQUNKLEVBQUU7WUFDckIsMERBQTBEO1lBQzFELE1BQU0sY0FBYyxHQUF3QjtnQkFDMUMsT0FBTyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDaEMsT0FBTyxFQUFFO29CQUNQLFlBQVksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxnQkFBZ0I7aUJBQ3BEO2dCQUNELElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSztnQkFDaEIsT0FBTyxFQUFFLEtBQUs7YUFDZixDQUFDO1lBQ0YsTUFBTSxnQkFBZ0IscUJBQVEsY0FBYyxFQUFLLElBQUksQ0FBRSxDQUFDO1lBQ3hELE9BQU8sZ0JBQWdCLENBQUM7UUFDMUIsQ0FBQyxDQUFDO1FBeklBLElBQUk7WUFDRixJQUFJLE1BQU0sQ0FBQyxhQUFhLEVBQUU7Z0JBQ3hCLFlBQUUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ3JDO1lBQ0QsTUFBTSxhQUFhLEdBQWlCO2dCQUNsQyxpQkFBaUIsRUFBRSxNQUFNO2dCQUN6QixPQUFPLEVBQUUsV0FBVztnQkFDcEIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsT0FBTyxFQUFFLElBQUk7YUFDZCxDQUFDO1lBQ0YsSUFBSSxDQUFDLE1BQU0scUJBQVEsYUFBYSxFQUFLLE1BQU0sQ0FBRSxDQUFDO1lBQzlDLCtFQUErRTtZQUMvRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEtBQUssTUFBTSxFQUFFO2dCQUM1QyxJQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLHNCQUFjLEVBQUUsQ0FBQzthQUMxRDtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxjQUFJLENBQUMsS0FBSyxDQUFDO2dCQUMxQixTQUFTLEVBQUUsSUFBSTtnQkFDZixVQUFVLEVBQUUsRUFBRTthQUNmLENBQUMsQ0FBQztTQUNKO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDVixNQUFNLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3BCO0lBQ0gsQ0FBQztDQW1IRjtBQW5KRCx3QkFtSkMifQ==